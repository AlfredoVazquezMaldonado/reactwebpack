var PROD = JSON.parse(process.env.PROD_ENV || '0');
var webpack = require('webpack');

module.exports = {
	entry: './index.js',
	output: {
		filename: PROD ? 'bundle.min.js' : 'bundle.js',
		path: './'
	},
	module: {
		loaders: [{
			test: /\.js$/,
			exclude: /node_modules/,
			loader: "babel-loader"
		}]
	},
	plugins: PROD ? [
		new webpack.optimize.UglifyJsPlugin({
			compress: {
				warnings: false
			}
		})
	] : []
}
